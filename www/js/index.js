var app = {
    dict: [],
    history: [],

    loadDictionary: function (wwwPath) {
        var request = new XMLHttpRequest();
        request.open("GET", wwwPath, true);

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                if (request.status == 200 || request.status == 0) {
                    app.dict = JSON.parse(request.responseText);

                    var box = app.getSearchBox();
                    box.disabled = false;
                    box.value = "";

                    app.showSuggestions("");
                }
            }
        }

        request.send();
    },

    initialize: function() {
        this.bindEvents();
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

        app.getSearchBox().addEventListener('keyup', this.onSearchBoxKeyUp);
        document.getElementById('goBack').addEventListener('click', app.goBack);
    },

    // PhoneGap is ready
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // app.receivedEvent('deviceready');

        app.loadDictionary('db/dict.json');
    },

    /*
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    */

    addClass: function (el, classNameToAdd) {
        var elClass = ' ' + el.className + ' ';

        if (elClass.indexOf(' ' + classNameToAdd + ' ') == -1) {
            el.className += ' ' + classNameToAdd;
        }
    },

    removeClass: function (el, classNameToRemove) {
        var elClass = ' ' + el.className + ' ';

        while (elClass.indexOf(' ' + classNameToRemove + ' ') !== -1) {
            elClass = elClass.replace(' ' + classNameToRemove + ' ', '');
        }

        el.className = elClass;
    },

    getDict: function() {
        return app.dict;
    },

    getResultBox: function() {
        return document.getElementById('searchResult');
    },

    getSearchBox: function() {
        return document.getElementById('searchBox');
    },

    goBack: function() {
        if (app.history.length > 1) {
            app.history.pop();
            var word = app.history.pop(); // read previous word
            console.log("Moving back to " + word + ", new history:");
            console.log(app.history);

            app.showMeaning(null, word);
        }
    },

    showMeaning: function(event, word) {
        if ((word == undefined) || (word == null)) {
            // word not supplied, assume it was a link tapped so recover the word from the element
            var source = event.target || event.srcElement;
            word = source.getAttribute('href').substring(1).toLowerCase(); // remove # character from the link
        }

        var box = app.getResultBox();
        app.removeClass(box, 'suggestions');

        var dict = app.getDict();

        if (dict[word]) {
            // update content and search string
            box.innerHTML = dict[word].join("\n");
            app.getSearchBox().value = word;

            // if we didn't just return from the history, add the current word to it
            app.history.push(word);
            console.log("New history:");
            console.log(app.history);
        } else {
            box.innerHTML = "Термин не найден";
        }

        app.processLinks();


    },

    showSuggestions: function (word) {
        var box = app.getResultBox();
        app.addClass(box, 'suggestions');

        var dict = app.getDict();
        var suggestions = [];

        // prepare a list of suggestions
        for (var key in dict) {
            if (key.substr(0, word.length) == word) {
                suggestions.push('<a href="#' + key + '" class="suggestion">' + key + '</a>');
            }
        }

        if (suggestions.length == 0) {
            box.innerHTML = "Термин не найден";
        } else {
            box.innerHTML = suggestions.join("<br/>\n");
        }

        app.processLinks();
    },

    processLinks: function() {
        var suggestions = document.getElementsByClassName('suggestion');

        for (var i = 0, len = suggestions.length; i < len; i++) {
            suggestions[i].addEventListener('click', app.showMeaning);
        }
    },

    onSearchBoxKeyUp: function() {
        var word = app.getSearchBox().value.toLowerCase();
        var dict = app.getDict();

        if (dict[word]) {
            app.showMeaning(null, word);
        } else {
            app.showSuggestions(word);
        }
    }
};
